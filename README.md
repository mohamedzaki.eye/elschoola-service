# El-Schoola Service

| **Laravel**  |  **service** |
|---|---|
| 7.0  | ^1.0 |

`elschoola/service` is a Laravel package which manage your application installation and update system. This package is supported and tested in Laravel 7.

## Requirements
- [PHP >= 7.2](http://php.net/)
- [Laravel 7|8](https://github.com/laravel/framework)


## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
